// ==UserScript==
// @id             iitc-plugin-aplus-player-tracker
// @name           IITC plugin: A+ Player Tracker
// @category       Layer
// @version        0.2.1.20150918.140900
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://www.c-aces.com/iitc/aplus-player-tracker.meta.js
// @downloadURL    https://www.c-aces.com/iitc/aplus-player-tracker.user.js
// @description    [IRDE-2015-04-03-133314] Draw trails for the path a user took and other information (created/destroyed links, resos, and fielding MU) onto the map based on status messages in COMMs. Uses up to three hours of data. Does not request chat data on its own, even if that would be useful.
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @include        https://www.ingress.com/mission/*
// @include        http://www.ingress.com/mission/*
// @match          https://www.ingress.com/mission/*
// @match          http://www.ingress.com/mission/*
// @grant          none
// ==/UserScript==